// Variables
var num1 = 5;
var num2 = 10;
var suma = num1+num2;

console.log(suma);

// let
let mensaje = "Alumna: Luciana Montaña 5to 2da";

console.log(mensaje);

//Constantes
const nombre = "Luciana";
const saludo = "Bienvenida ";
const mensaje2 = saludo+nombre;

console.log(mensaje2);

// Calculos Matematicos
function sumar(){
    var s1 = parseInt(document.getElementById('num1').value);
    var s2 = parseInt(document.getElementById('num2').value);
    document.getElementById('resultado').innerHTML = s1 + s2 ;
}

function restar(){
    var r1 = parseInt(document.getElementById('num1').value);
    var r2 = parseInt(document.getElementById('num2').value);
    document.getElementById('resultado').innerHTML = r1 - r2 ;
}

function multiplicar(){
    var m1 = parseInt(document.getElementById('num1').value);
    var m2 = parseInt(document.getElementById('num2').value);
    document.getElementById('resultado').innerHTML = m1 * m2 ;
}

function dividir(){
    var d1 = parseInt(document.getElementById('num1').value);
    var d2 = parseInt(document.getElementById('num2').value);
    document.getElementById('resultado').innerHTML = d1 / d2 ;
}

// Array
Array = ['Luciana','Belen','Montaña'];
console.log('Arreglo:');
for(i=0;i<Array.length;i++){
    console.log(Array[i]);
}